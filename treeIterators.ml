
type 'a node = {
  key: 'a;
  left: 'a t;
  right: 'a t;
}

and 'a t =
  | Empty
  | Node of 'a node

let rec fold ~f tree init =
  match tree with
  | Empty -> init
  | Node node ->
    init
    |> fold ~f node.left
    |> f node.key 
    |> fold ~f node.right

let cardinal tree = fold ~f:(fun _ i -> i+1) tree 0


module type PairFinder =
sig
  val name : string 
  val solve : int -> int t -> (int * int) option
end

module UsingLists =
struct
  let name = "stdlib lists"

  let decreasing_elements tree =
    fold ~f:(fun key list -> key::list) tree []

  let rec find_pair sum increasing decreasing =
    match increasing, decreasing with
    | x::bigger, y::smaller when x > y -> None (* dont try each pair twice *)
    | x::bigger, y::smaller when x + y = sum ->
      Some (x,y) 
    | x::bigger, y::smaller when x + y < sum ->
      find_pair sum bigger decreasing
    | x::bigger, y::smaller (* when x + y > sum *) ->
      find_pair sum increasing smaller
    | _ -> None

  let solve sum tree =
    let decreasing = decreasing_elements tree in
    let increasing = List.rev decreasing in
    find_pair sum increasing decreasing 

end


module UsingLazyLists : PairFinder =
struct
  let name = "custom lazy lists"
  
  type 'elt lazy_cell =
    | Nil
    | Cons of 'elt * 'elt lazy_list
  and 'elt lazy_list = 'elt lazy_cell Lazy.t


  let lazy_increasing_elements tree =
    let rec loop accu = function
      | Empty ->
        accu
      | Node node ->
        loop (Cons (node.key, lazy (loop accu node.right))) node.left
    in
    lazy (loop Nil tree)

  
  let rec lazy_decreasing_elements tree =
    let rec loop accu = function
      | Empty ->
        accu
      | Node node ->
        loop (Cons (node.key, lazy (loop accu node.left))) node.right
    in
    lazy (loop Nil tree)


  let rec find_pair sum increasing decreasing =
    match Lazy.force increasing, Lazy.force decreasing with
    | Cons (elt1,tail1), Cons (elt2,tail2) when elt2 < elt1 ->
      None
    | Cons (elt1,tail1), Cons (elt2,tail2) when elt1 + elt2 = sum ->
      Some (elt1,elt2)
    | Cons (elt1,tail1), Cons (elt2,tail2) when elt1 + elt2 > sum ->
      find_pair sum  increasing tail2
    | Cons (elt1, tail1), Cons (elt2,tail2) (* when elt1 + elt2 < sum *) ->
      find_pair sum tail1 decreasing
    | _ -> None


  let solve sum tree =
    find_pair sum
      (lazy_increasing_elements tree)
      (lazy_decreasing_elements tree)
  
end


module UsingSimiliLazyLists : PairFinder =
struct
  let name = "simili lazy lists"
  
  type 'elt lazy_cell =
    | Nil
    | Cons of 'elt * 'elt lazy_list
  and 'elt lazy_list = unit -> 'elt lazy_cell

                   
  let lazy_increasing_elements tree =
    let rec loop accu = function
      | Empty ->
        accu
      | Node node ->
        loop (Cons (node.key, (fun () -> loop accu node.right))) node.left
    in
    fun () -> loop Nil tree

  
  let rec lazy_decreasing_elements tree =
    let rec loop accu = function
      | Empty ->
        accu
      | Node node ->
        loop (Cons (node.key, (fun () -> loop accu node.left))) node.right
    in
    fun () -> loop Nil tree



  let rec find_pair sum increasing decreasing =
    match increasing (), decreasing () with
    | Cons (elt1,tail1), Cons (elt2,tail2) when elt2 < elt1 ->
      None
    | Cons (elt1,tail1), Cons (elt2,tail2) when elt1 + elt2 = sum ->
      Some (elt1,elt2)
    | Cons (elt1,tail1), Cons (elt2,tail2) when elt1 + elt2 > sum ->
      find_pair sum  increasing tail2
    | Cons (elt1, tail1), Cons (elt2,tail2) (* when elt1 + elt2 < sum *) ->
      find_pair sum tail1 decreasing
    | _ -> None


  let solve sum tree =
    find_pair sum
      (lazy_increasing_elements tree)
      (lazy_decreasing_elements tree)
  
end


module UsingGenerators : PairFinder =
struct 
  let name = "generators"
  
  let increasing_generator tree =
    let rec left_descent stack = function
      | Empty -> stack
      | Node node -> left_descent (node::stack) node.left
    in 
    let stack = ref (left_descent [] tree) in
    fun () ->
      match !stack with
      | [] -> None
      | head::tail ->
        stack := left_descent tail head.right;
        Some head.key

  let decreasing_generator tree =
    let rec right_descent stack = function
      | Empty -> stack
      | Node node -> right_descent (node::stack) node.right
    in
    let stack = ref (right_descent [] tree) in
    fun () ->
      match !stack with
      | [] -> None
      | head::tail ->
        stack := right_descent tail head.left;
        Some head.key

  let solve sum tree =
    let next_increasing = increasing_generator tree in
    let next_decreasing = decreasing_generator tree in
    let rec loop left_value right_value =
      match left_value, right_value with
      | Some left, Some right when left > right ->
        None
      | Some left, Some right when left + right = sum ->
        Some (left,right)
      | Some left, Some right when left + right > sum ->
        loop left_value (next_decreasing ())
      | Some left, Some right (* when left + right < sum *) ->
        loop (next_increasing ()) right_value
      | _ -> None 
    in
    loop (next_increasing ()) (next_decreasing ())
        

  
end


module UsingZippers : PairFinder =
struct
  let name = "zippers"
  
  type 'elt path = 
    | LeftChild of 'elt * 'elt t * 'elt path
    | RightChild of 'elt t * 'elt * 'elt path 
    | Root 
  type 'elt zipped_tree = 'elt node * 'elt path

  let of_node node = (node, Root)
                
  let go_left ((node, path) as zipper) =
    match node.left with
    | Node left_child ->
      (left_child, LeftChild (node.key, node.right, path))
    | Empty -> zipper
  let go_right ((node,path) as zipper) =
    match node.right with
    | Node right_child ->
      (right_child, RightChild(node.left, node.key, path))
    | Empty -> zipper
  let go_up = function
    | (left_child, LeftChild (key, right, path)) ->
      ({ left = Node left_child; key; right }, path)
    | (right_child, RightChild (left, key, path)) ->
      ({left; key; right = Node right_child }, path)
    | root -> root

  let can_go_left (node, path) = node.left <> Empty
  let can_go_right (node, path) = node.right <> Empty
  let is_root (tree, path) = path = Root
  let is_right = function
    | ( _, RightChild (_,_,_)) -> true
    | _ -> false
  let is_left = function
    | ( _, LeftChild (_,_,_)) -> true
    | _ -> false
      
  let rec go_leftmost zipper =
    if can_go_left zipper then
       zipper |> go_left |> go_leftmost
    else
      zipper
        
  let rec go_rightmost zipper =
    if can_go_right zipper then
      zipper |> go_right |> go_rightmost
    else
      zipper

  let rec go_up_leftmost zipper =
    if is_right zipper then
      zipper |> go_up |> go_up_leftmost
    else
      zipper

  let rec go_up_rightmost zipper =
    if is_left zipper then
      zipper |> go_up |> go_up_rightmost
    else
      zipper

  let try_go_up_left zipper =
    if is_right zipper then Some (go_up zipper)
    else None

  let try_go_up_right zipper =
    if is_left zipper then Some (go_up zipper)
    else None
  
  let rec go_to_next zipper =
    if can_go_right zipper then
      Some (zipper |> go_right |> go_leftmost)
    else
      zipper |> go_up_leftmost |> try_go_up_right

  
  let go_to_previous zipper =
    if can_go_left zipper then
      Some (zipper |> go_left |> go_rightmost)
    else
      zipper |> go_up_rightmost |> try_go_up_left

  let current_key (node,path) = node.key

  let (>>=) opt fct =
    match opt with
    | None -> None
    | Some e -> fct e
  
  let rec find_pair sum increasing_zipper decreasing_zipper =
    match current_key increasing_zipper, current_key decreasing_zipper with
    | left, right when left > right -> None
    | left, right when left + right = sum ->
      Some (left, right)
    | left, right when left + right > sum ->
      go_to_previous decreasing_zipper >>= fun decreasing_zipper ->
      find_pair sum increasing_zipper decreasing_zipper
    | left, right (* when left + right < sum *) ->
      go_to_next increasing_zipper >>= fun increasing_zipper ->
      find_pair sum increasing_zipper decreasing_zipper
  
  let solve sum = function
    | Empty -> None
    | Node node ->
      find_pair sum
        (of_node node |> go_leftmost)
        (of_node node |> go_rightmost)
    
end


module CreateInstances =
struct
  
  let rec insert key = function
    | Empty -> Node { key; left = Empty; right = Empty }
    | Node node when key < node.key ->
      Node { node with left = insert key node.left }
    | Node node when key > node.key ->
      Node { node with right = insert key node.right }
    | tree -> tree 


  let of_list list =
    List.fold_left
      (fun tree key -> insert key tree)
      Empty
      list

  let rec repeat f init n =
    if n = 0 then init
    else repeat f (f init) (n-1)

  let list_init f n =
    let rec loop i list =
      if i < 0 then list
      else loop (i-1) (f i :: list)
    in loop n []
                      
  
  let range low high =
    let rec loop i list =
      if i < low then list
      else loop (i-1) (i::list)
    in loop high []

  let rec shuffle = function
    | [] -> []
    | [single] -> [single]
    | list ->
      let (half1, half2) = List.partition (fun _ -> Random.bool ()) list in
      List.rev_append (shuffle half1) (shuffle half2)
  let () = Random.self_init ()
    
  let random_range_tree n = range 1 n |> shuffle |> of_list

  let fully_random_list ?(mini=0) ?(maxi=max_int) n =
     list_init (fun _ -> mini + Random.int (maxi - mini)) n 

  let satisfiable_instance ~ratio ~n =
    assert (ratio > 0. && ratio <= 0.5);
    let nb_sides = int_of_float (ratio *. float n) in
    let nb_middle = max 0 (n - 2 * nb_sides) in
    let below =
      fully_random_list ~mini:0 ~maxi:n nb_sides
      |> List.map (( * ) 4)
    in
    let above =
      fully_random_list ~mini:(2*n) ~maxi:(3*n) nb_sides
      |> List.map (( * ) 4)
    in
    let middle =
      fully_random_list ~mini:n ~maxi:(2*n) nb_middle
      |> List.map (( * ) 4)
    in
    let left = 4 * n + 1 in
    let right = 8 * n + 1 in (* only this pair has sum = 2 mod 4 *)
    let all = below @ (left :: middle) @ (right :: above) in 
    (left, right, left+right, all |> shuffle |> of_list)
  

end


let implementations =
  [ (module UsingLists : PairFinder);
    (module UsingLazyLists : PairFinder);
    (module UsingSimiliLazyLists : PairFinder);
    (module UsingGenerators : PairFinder);
    (module UsingZippers : PairFinder)
  ]


let check_instance (module Impl : PairFinder) (left,right,target,tree) =
  match Impl.solve target tree with
  | Some (l,r) -> assert (l = left && r = right)
  | None -> assert false
    

let benchmark_module (module Impl : PairFinder) instances =
  Core_bench.Test.create ~name:Impl.name
    (fun () -> List.iter (check_instance (module Impl)) instances)
    

let benchmarks ~ratio ~size ~nb_instances =
  let instances =
    let open CreateInstances in
    list_init (fun _ -> satisfiable_instance ~ratio ~n:size) nb_instances
  in 
  List.map
    (fun impl -> benchmark_module impl instances)
    implementations


let main =
  let size = 10000 in
  let ratio = 0.2 in
  let nb_instances = 2 in 
  benchmarks ~ratio ~size ~nb_instances
  |> Core_bench.Bench.make_command
  |> Core.Command.run 

